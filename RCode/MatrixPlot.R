#Packages
#install.packages(c('tidyverse','leaflet'))
library('tidyverse')
library("leaflet")


#Setting the working directory to /data so we can use the data in there
#Requires you to use rstudio, and to run from the .R file (rather than the console)
setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
setwd('..')
setwd("./PythonCode/Data")

#Reading in the demand file
Demand = read.csv("demandWithStore.csv", header=TRUE)
dem_tib = as_tibble(Demand)
#Stripping out the specific store, leaving only the brand (e.g new world, four square etc)
dem_tib = select(dem_tib,-ends_with("market"))

#Tidying the data
dem_tidy = gather(dem_tib, "Date", "Demand", 2:29)


#Formatting the date to go from "dd.mm.yyyy" to the day of the week
dem_tidy$Date = sub('.','',dem_tidy$Date)
dem_tidy$Date = as.factor(weekdays(as.Date(dem_tidy$Date, '%d.%m.%Y')))

#Removing Sunday (as it always == 0)
dem_tidy = filter(dem_tidy, Date != 'Sunday')
#Changing the order of the levels so that the matrix of graphs is in the correct order
dem_tidy$Date <- factor(dem_tidy$Date, levels = c("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"))

dem_week = dem_tidy
#Gathering all weekdays together under the label "Weekday"
levels(dem_week$Date) = list("Weekday" = c("Monday", "Tuesday", "Wednesday", "Thursday", "Friday"), "Saturday" = "Saturday")


#Producing the matrix of histograms for all days via the facet_grid function
t <- ggplot(dem_tidy, aes(x=Demand)) + geom_histogram()
t = t + facet_grid(rows = vars(Brand), cols = vars(Date)) + ggtitle('Matrix of demand by weekday by store')
#Setting the working directory to the visualisations folder for saving
setwd('..')
setwd('..')
setwd("./Visualisations")
ggsave("MatrixOfDemand.png")
t

#Producing and saving the matrix of histrograms for the Weekdays vs Saturdays demand
z <- ggplot(dem_week, aes(x=Demand)) + geom_histogram()
z = z + facet_grid(rows = vars(Brand), cols = vars(Date)) + ggtitle('Matrix of demand by weekday by store')
setwd('..')
setwd("./Visualisations")
ggsave("MatrixOfDemandSaturday.png")
z


#Creating an 8x3 dataframe to store the 75th Percentile for each combo of brand and weekday/saturday
PTile_df = dem_week %>% distinct(Brand,Date, .keep_all= TRUE)
PTile_df$Demand = 0 #Zeroing out the demands



#Getting Percentiles
#Iterating through all the stores
for (store in levels(dem_week$Brand)){
  #Iterating through the 2 day types
  for (day in levels(dem_week$Date)){
    
    #Creating a filtered dataset of only 1 brand on 1 day
    dem_Filtered=filter(dem_week, Brand==store, Date==day) 
    
    PTile=quantile(dem_Filtered$Demand,0.75)
    
    #Creating and printing 
    #PTile_String = paste("75th Percentile of store", store, "On ", day, "is: ", PTile)
    #print(PTile_String)
    
    #Creating a string to form the caption
    PTile_String2 = paste("75th Percentile =", PTile)
    
    #Finding the correct row to assign in the datafram the stores the percentile values
    RowNum = which((PTile_df$Brand == store) & (PTile_df$Date == day))
    PTile_df$Demand[RowNum] <- PTile
    PTile_df$Caption[RowNum] <- PTile_String2
    
  }
}

#Adding the percentiles to the plot as a vertical line
z = z+ geom_vline(data=PTile_df, aes(xintercept=PTile_df$Demand))

#Captioning the plot
z = z+geom_text(
  data = PTile_df,
  mapping = aes(x = Inf, y = Inf, label = Caption),
  hjust = 1.05,
  vjust=1.5
)

z


setwd('..')
setwd("./Visualisations")
ggsave("MatrixOfDemandWithLine.png")

setwd('..')
setwd("./PythonCode/ProjectCode")
write.csv(dem_week, file="DemandForSim.csv")

