
from pulp import*
import pandas as pd
import numpy as np
from IsOnTheRoute import*
import os

def lp_form(Routes, Mins):
    '''
    A function to take a set of feasible Routes and find the least cost set of routes that encompasses all supermarkets
    Inputs:
        Routes: a list of all the feasible routes
        Mins: a list of the corresponding travel times of these routes
    Outputs:
        actualroutes: a list of the optimal routes
    '''
    
    cwd = os.getcwd()
    #Reading in the names of all the supermarkets
    locations = pd.read_csv(cwd + os.sep+ "PythonCode" + os.sep + "ProjectCode" + os.sep + "FoodstuffLocations.csv")
    locations = locations['Supermarket'] # equivalent to patterns
    print("\nMaximum time taken: ", np.max(Mins))
    #variable to store the costs
    costs = []
 

    #create a variable 
    x = len(Mins)    
    Mins1 = Mins
    Mins = Mins + Mins1

    # use the travel times to get the costs 
    for i in range(len(Mins)):
        if ((Mins[i] <= 4*60) and (i < x)):
            #We charge in 15 minute increments, so round up to the next 15 minute block
            #And then divide by 4 to find the number of hours
            costs.append((np.ceil(Mins[i]/15))/4*150)
        #    print ("Cost of route ", Routes[i], "is: ", cost[i])
        elif (i<x):
            #We charge in 15 minute increments so divide by 15, and then round up to get the number of blocks
            #We then divide the number of 15 minute blocks by 4, to find the number of hours
            #And multiply this by the hourly cost.
            costs.append(( (np.ceil(Mins[i]/15)/4) - 4)*200 + 150*(4))
        elif((i>=x) and (Mins[i]*60 <= 4*60*60)): # these add to the cost for the mainfreight trucks
            costs.append(1200)
        else:
            costs.append(2400)
    # Add the mainfreoght routes
    Routes1 = Routes[:]
    Routes = Routes + Routes1
    i = 0

    # create a matrix that has all the routes with all the stores so we can formulate the lp
    matrix = np.zeros((len(locations),len(Routes)))
    i = 0
    j = 0
    locations = locations[locations != 'Warehouse'] # take warehouse out of the nodes
    for route in Routes:
        j = 0
        for location in locations:
            if IsOnTheRoute(route, location):
                matrix[j][i] = 1
            j+=1
        i+=1
    
    #create a list of all the route names indexing by numbers starting at 0
    RouteNames = []
    for i in range(len(Routes)):
        if (i<x):
            RouteNames.append(str(i))
        if (i>=x):
            RouteNames.append("m-"+str(i-x))
    #create a list of all of them just with the non mainfreight trucks
    RouteNamesClean = []
    for i in range(len(Routes)):
        if (i<x):
            RouteNamesClean.append(str(i))


    # create the variables
    vars = LpVariable.dicts("route", RouteNames, 0, 1, LpBinary)

    # create the problem
    prob = LpProblem("Team18 LP", LpMinimize)

    # create the objective function
    prob += lpSum([vars[i]*costs[j] for i,j in zip(RouteNames,range(len(costs)))])
    
    # add the route constraints, must visit every node only once
    for i in range(len(locations)):
        prob += lpSum([vars[j]*matrix[i][k] for k,j in zip(range(len(Routes)),RouteNames)]) == 1

    # add the constraint for only having 20 trucks
    prob += lpSum(vars[i] for i in RouteNamesClean) <= 20
    
    #write the problem to an lp file
    prob.writeLP("Team18 Lp")
    
    #solve the problem
    prob.solve()

    # pull out the optimal routes
    opt_Routes = []    
    for v in prob.variables():
        if v.varValue == 1:
            opt_Routes.append(v)

    print(opt_Routes)
    #print how many routes we have
    print("Number of optimal routes =", len(opt_Routes))
   
    # from the optimal routes, get the route numbers
    Optimal_routes = []
    for v in opt_Routes:
        x, y = v.name.rsplit("_",1)
        Optimal_routes.append(int(y))
   
    #print all the route numbers
    print(Optimal_routes)
    j = 0
    #print the routes
    for i in Optimal_routes:
        if j <20:
            print(Routes[i])
            j+=1
        else:
            print(Routes[i], " - Mainfreight route")
            j+=1

    # print the total cost
    print("Cost =", value(prob.objective))
    actualroutes = []
    for i in Optimal_routes:
        actualroutes.append(Routes[i])
    
    #Creating a list of the times for the optimal routes, indexed to match actualroutes
    Optimal_Times = [Mins[i] for i in Optimal_routes]
    
    return actualroutes, Optimal_Times, value(prob.objective)
   
    