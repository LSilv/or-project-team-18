import pandas as pd
import numpy as np
import os

def truck_NS(Day):
    '''
    This function partitions the locations to the north and south of the warehouse and uses a greedy algorithm to 
    generate feasible routes
    Input: 
        Day: Whether demand should be calculated for Weekdays or Saturdays
    Output: 
        Routes: A list of all the unique feasible routes generated
        Mins: A list of the corresponding route times 
    '''

    cwd = os.getcwd()
    locations = pd.read_csv(cwd + os.sep+ "PythonCode" + os.sep + "ProjectCode" + os.sep + "FoodstuffLocations.csv")
    traveltimes = pd.read_csv(cwd + os.sep+ "PythonCode" + os.sep + "ProjectCode" + os.sep + "FoodstuffTravelTimes.csv")
    Routes = [] # keeps track of all routes
    Mins = [] # keeps trank of all mins
    totalHours = 0 # toatal hours

    locationsNorth = locations[(locations['Lat']>-36.90790)] #Shops north of warehouse
    locationsSouth = locations[(locations['Lat']<-36.90790)] #shops south of warehouse 
    locationsSouth = locationsSouth[locationsSouth['Supermarket'] != 'Warehouse'] # remove the warehouse from the list
    i=0 # counts how many routes
   

    while (len(locationsNorth) != 0): #while ther are still unvisited nodes
        
        currentNode = 'Warehouse' #set node as warehouse, ie beginnning new trip.
        palletsDelivered=0 #set pallets delivered in this trip to 0 (new trip)
        i+=1 # count the routes
        Route = [] # keeps track of the route
        timeLeft = 4*60*60 #The time left in the route for driving only
        RouteTime = 0 # total route time
        Bool = True # whether to continue looping
        Route.append(currentNode) # start the route at the warehouse

        while (palletsDelivered != 12) & (Bool == True): # keep going untill you have 12 pallets or there are no more places to go
            Bool = False
            #find shortest route from current node to next node, which wont go over 12
            minm = 10000 #currentNode is a string of the node name
            to_node = currentNode #set initial/default minimum traveltime value and to-location 
            

            for location in locationsNorth['Supermarket']: #go through all locations, location is supermarket name, same format as current node.
                if ((traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] + 60*5*locations[Day][traveltimes.columns.get_loc(location)-1]) < timeLeft) & (traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] < minm) and (palletsDelivered + locations[Day][traveltimes.columns.get_loc(location)-1] <= 12) and ((currentNode!=location)==True):
                    minm = traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] # set this to min
                    to_node = location # set this node to to_node
                    Bool = True # keep looping



            if Bool == True: # if we found a new node to go to add it to everything
                palletsDelivered += locations[Day][traveltimes.columns.get_loc(to_node)-1]
                timeLeft -= minm #Subtracting the driving time
                timeLeft -= 5*60*locations[Day][traveltimes.columns.get_loc(to_node)-1] #Subtracting the unloading time
                Route.append(to_node)# append the new node to the route
                RouteTime += minm # add it to the route time
                RouteTime += 5*60*locations[Day][traveltimes.columns.get_loc(to_node)-1] # add the unloading time


            currentNode = to_node# change the current node so we can look from there
            locationsNorth = locationsNorth[locationsNorth['Supermarket'] != currentNode] #use conditional indexinf to remove past node from set.

            if (palletsDelivered == 12): # if we have a full truck, stop
                Bool = False


            if (Bool == False): # if we have finished the route, head back to the warehouse
                Route.append('Warehouse')
                RouteTime += traveltimes[to_node][traveltimes.columns.get_loc('Warehouse')-1]
                totalHours += RouteTime/60/60
                #append the route to the list of all routes
                Routes.append(Route)
                Mins.append(RouteTime/60)



    ## south
    while (len(locationsSouth) != 0): #while ther are still unvisited nodes
        
        currentNode = 'Warehouse' #set node as warehouse, ie beginnning new trip.
        palletsDelivered=0 #set pallets delivered in this trip to 0 (new trip)
        i+=1 # count the routes
        Route = [] # keeps track of the route
        timeLeft = 4*60*60 #The time left in the route for driving only
        RouteTime = 0 # total route time
        Bool = True # whether to continue looping
        Route.append(currentNode) # start the route at the warehouse

        while (palletsDelivered != 12) & (Bool == True):
            Bool = False
            #find shortest route from current node to next node, which wont go over 12
            minm = 10000 #currentNode is a string of the node name
            to_node = currentNode #set initial/default minimum traveltime value and to-location 
            

            for location in locationsSouth['Supermarket']: #go through all locations, location is supermarket name, same format as current node.
                if ((traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] + 60*5*locations[Day][traveltimes.columns.get_loc(location)-1]) < timeLeft) & (traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] < minm) and (palletsDelivered + locations[Day][traveltimes.columns.get_loc(location)-1] <= 12) and ((currentNode!=location)==True):
                    minm = traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] #set this value to the min
                    to_node = location # set this node to the node
                    Bool = True # continue looping



            if Bool == True:  # if we found a new node to go to add it to everything
                palletsDelivered += locations[Day][traveltimes.columns.get_loc(to_node)-1] # add the pallets
                timeLeft -= minm #Subtracting the driving time
                timeLeft -= 5*60*locations[Day][traveltimes.columns.get_loc(to_node)-1] #Subtracting the unloading time
                Route.append(to_node) # add the node to the route
                RouteTime += minm # add to the route time
                RouteTime += 5*60*locations[Day][traveltimes.columns.get_loc(to_node)-1] # add the unloading time


            currentNode = to_node # set this node to the current node
            locationsSouth = locationsSouth[locationsSouth['Supermarket'] != currentNode] #use conditional indexinf to remove past node from set.
    
            if (palletsDelivered == 12): # if the truck is full stop looping
                Bool = False 

            if (Bool == False): # if the route is finished head back to the warehouse
                Route.append('Warehouse')
                RouteTime += traveltimes[to_node][traveltimes.columns.get_loc('Warehouse')-1]
                totalHours += RouteTime/60/60
                Routes.append(Route) # append this route to the list of all routes
                Mins.append(RouteTime/60)

    return Routes, Mins


def truck_None(Day):
    '''
    This function uses a greedy algorithm to generate feasible routes
    Input: 
        Day: Whether demand should be calculated for Weekdays or Saturdays
    Output: 
        Routes: A list of all the unique feasible routes generated
        Mins: A list of the corresponding route times 
    '''

    cwd = os.getcwd()
    locations = pd.read_csv(cwd + os.sep+ "PythonCode" + os.sep + "ProjectCode" + os.sep + "FoodstuffLocations.csv")
    traveltimes = pd.read_csv(cwd + os.sep+ "PythonCode" + os.sep + "ProjectCode" + os.sep + "FoodstuffTravelTimes.csv")
    Routes = [] # keeps track of all routes
    Mins = [] # keeps trank of all mins
    totalHours = 0
    i=0
    
    locations = locations[locations['Supermarket'] != 'Warehouse'] # take warehouse out of the nodes

    while (len(locations) != 0): #while ther are still unvisited nodes
        
        currentNode = 'Warehouse' #set node as warehouse, ie beginnning new trip.
        palletsDelivered=0 #set pallets delivered in this trip to 0 (new trip)
        i+=1 # count the routes
        Route = [] # keeps track of the route
        timeLeft = 4*60*60 #The time left in the route for driving only
        RouteTime = 0 # total route time
        Bool = True # whether to continue looping
        Route.append(currentNode) # start the route at the warehouse

        while (palletsDelivered != 12) & (Bool == True): # keep going untill you have 12 pallets or there are no more places to go
            Bool = False
            #find shortest route from current node to next node, which wont go over 12
            minm = 10000 #currentNode is a string of the node name
            to_node = currentNode #set initial/default minimum traveltime value and to-location 
            

            for location in locations['Supermarket']: #go through all locations, location is supermarket name, same format as current node.
                #    print(location)
                if ((traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] + 60*5*locations[Day][traveltimes.columns.get_loc(location)-1]) < timeLeft) & (traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] < minm) and (palletsDelivered + locations[Day][traveltimes.columns.get_loc(location)-1] <= 12) and ((currentNode!=location)==True):
                    minm = traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] # set this to min
                    to_node = location # set this node to to_node
                    Bool = True # keep looping



            if Bool == True: # if we found a new node to go to add it to everything
                palletsDelivered += locations[Day][traveltimes.columns.get_loc(to_node)-1]
                timeLeft -= minm #Subtracting the driving time
                timeLeft -= 5*60*locations[Day][traveltimes.columns.get_loc(to_node)-1] #Subtracting the unloading time
                Route.append(to_node)# append the new node to the route
                RouteTime += minm # add it to the route time
                RouteTime += 5*60*locations[Day][traveltimes.columns.get_loc(to_node)-1] # add the unloading time


            currentNode = to_node# change the current node so we can look from there
            locations = locations[locations['Supermarket'] != currentNode] #use conditional indexinf to remove past node from set.
    

            if (palletsDelivered == 12): # if we have a full truck, stop
                Bool = False


            if (Bool == False): # if we have finished the route, head back to the warehouse
                Route.append('Warehouse')
                RouteTime += traveltimes[to_node][traveltimes.columns.get_loc('Warehouse')-1]
                totalHours += RouteTime/60/60
                #append the route to the list of all routes
                Routes.append(Route)
                Mins.append(RouteTime/60)

    return Routes, Mins


def truck_EW(Day):
    '''
    This function partitions the locations to the East and West of the warehouse and uses a greedy algorithm to 
    generate feasible routes
    Input: 
        Day: Whether demand should be calculated for Weekdays or Saturdays
    Output: 
        Routes: A list of all the unique feasible routes generated
        Mins: A list of the corresponding route times 
    '''

    cwd = os.getcwd()
    locations = pd.read_csv(cwd + os.sep+ "PythonCode" + os.sep + "ProjectCode" + os.sep + "FoodstuffLocations.csv")
    traveltimes = pd.read_csv(cwd + os.sep+ "PythonCode" + os.sep + "ProjectCode" + os.sep + "FoodstuffTravelTimes.csv")
    Routes = [] # keeps track of all routes
    Mins = [] # keeps trank of all mins
    totalHours = 0 # toatal hours

    locationsEast = locations[(locations['Long']>174.7281)] #Shops north of warehouse
    locationsWest = locations[(locations['Long']<174.7281)] #shops south of warehouse 
    locationsEast = locationsEast[locationsEast['Supermarket'] != 'Warehouse'] # remove the warehouse from the list
    i=0 # counts how many routes
   

    while (len(locationsEast) != 0): #while ther are still unvisited nodes
        
        currentNode = 'Warehouse' #set node as warehouse, ie beginnning new trip.
        palletsDelivered=0 #set pallets delivered in this trip to 0 (new trip)
        i+=1 # count the routes
        Route = [] # keeps track of the route
        timeLeft = 4*60*60 #The time left in the route for driving only
        RouteTime = 0 # total route time
        Bool = True # whether to continue looping
        Route.append(currentNode) # start the route at the warehouse

        while (palletsDelivered != 12) & (Bool == True): # keep going untill you have 12 pallets or there are no more places to go
            Bool = False
            #find shortest route from current node to next node, which wont go over 12
            minm = 10000 #currentNode is a string of the node name
            to_node = currentNode #set initial/default minimum traveltime value and to-location 
            

            for location in locationsEast['Supermarket']: #go through all locations, location is supermarket name, same format as current node.
                if ((traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] + 60*5*locations[Day][traveltimes.columns.get_loc(location)-1]) < timeLeft) & (traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] < minm) and (palletsDelivered + locations[Day][traveltimes.columns.get_loc(location)-1] <= 12) and ((currentNode!=location)==True):
                    minm = traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] # set this to min
                    to_node = location # set this node to to_node
                    Bool = True # keep looping



            if Bool == True: # if we found a new node to go to add it to everything
                palletsDelivered += locations[Day][traveltimes.columns.get_loc(to_node)-1]
                timeLeft -= minm #Subtracting the driving time
                timeLeft -= 5*60*locations[Day][traveltimes.columns.get_loc(to_node)-1] #Subtracting the unloading time
                Route.append(to_node)# append the new node to the route
                RouteTime += minm # add it to the route time
                RouteTime += 5*60*locations[Day][traveltimes.columns.get_loc(to_node)-1] # add the unloading time


            currentNode = to_node# change the current node so we can look from there
            locationsEast = locationsEast[locationsEast['Supermarket'] != currentNode] #use conditional indexinf to remove past node from set.

            if (palletsDelivered == 12): # if we have a full truck, stop
                Bool = False


            if (Bool == False): # if we have finished the route, head back to the warehouse
                Route.append('Warehouse')
                RouteTime += traveltimes[to_node][traveltimes.columns.get_loc('Warehouse')-1]
                totalHours += RouteTime/60/60
                #append the route to the list of all routes
                Routes.append(Route)
                Mins.append(RouteTime/60)



    ## south

    while (len(locationsWest) != 0): #while ther are still unvisited nodes
        
        currentNode = 'Warehouse' #set node as warehouse, ie beginnning new trip.
        palletsDelivered=0 #set pallets delivered in this trip to 0 (new trip)
        i+=1 # count the routes
        Route = [] # keeps track of the route
        timeLeft = 4*60*60 #The time left in the route for driving only
        RouteTime = 0 # total route time
        Bool = True # whether to continue looping
        Route.append(currentNode) # start the route at the warehouse

        while (palletsDelivered != 12) & (Bool == True):
            Bool = False
            #find shortest route from current node to next node, which wont go over 12
            minm = 10000 #currentNode is a string of the node name
            to_node = currentNode #set initial/default minimum traveltime value and to-location 
            

            for location in locationsWest['Supermarket']: #go through all locations, location is supermarket name, same format as current node.
                if ((traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] + 60*5*locations[Day][traveltimes.columns.get_loc(location)-1]) < timeLeft) & (traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] < minm) and (palletsDelivered + locations[Day][traveltimes.columns.get_loc(location)-1] <= 12) and ((currentNode!=location)==True):
                    minm = traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] #set this value to the min
                    to_node = location # set this node to the node
                    Bool = True # continue looping



            if Bool == True:  # if we found a new node to go to add it to everything
                palletsDelivered += locations[Day][traveltimes.columns.get_loc(to_node)-1] # add the pallets
                timeLeft -= minm #Subtracting the driving time
                timeLeft -= 5*60*locations[Day][traveltimes.columns.get_loc(to_node)-1] #Subtracting the unloading time
                Route.append(to_node) # add the node to the route
                RouteTime += minm # add to the route time
                RouteTime += 5*60*locations[Day][traveltimes.columns.get_loc(to_node)-1] # add the unloading time


            currentNode = to_node # set this node to the current node
            locationsWest = locationsWest[locationsWest['Supermarket'] != currentNode] #use conditional indexinf to remove past node from set.
    
            if (palletsDelivered == 12): # if the truck is full stop looping
                Bool = False 

            if (Bool == False): # if the route is finished head back to the warehouse
                Route.append('Warehouse')
                RouteTime += traveltimes[to_node][traveltimes.columns.get_loc('Warehouse')-1]
                totalHours += RouteTime/60/60
                Routes.append(Route) # append this route to the list of all routes
                Mins.append(RouteTime/60)

    return Routes, Mins

def truck_4(Day):
    '''
    This function partitions the locations to the north-East, North-West, South-West, and south-East of the warehouse and uses a greedy algorithm to 
    generate feasible routes
    Input: 
        Day: Whether demand should be calculated for Weekdays or Saturdays
    Output: 
        Routes: A list of all the unique feasible routes generated
        Mins: A list of the corresponding route times 
    '''


    cwd = os.getcwd()
    locations = pd.read_csv(cwd + os.sep+ "PythonCode" + os.sep + "ProjectCode" + os.sep + "FoodstuffLocations.csv")
    traveltimes = pd.read_csv(cwd + os.sep+ "PythonCode" + os.sep + "ProjectCode" + os.sep + "FoodstuffTravelTimes.csv")
    Routes = [] # keeps track of all routes
    Mins = [] # keeps trank of all mins
    totalHours = 0 # toatal hours
    Long = 174.725303
    Lat = -36.890425


    locationsNorthEast = locations[(locations['Long']>Long) & (locations['Lat'] > Lat)] #Shops north of warehouse
    locationsNorthWest = locations[(locations['Long']>Long) & (locations['Lat'] < Lat)] #shops south of warehouse
    locationsSouthEast = locations[(locations['Long']<Long) & (locations['Lat'] > Lat)] #Shops north of warehouse
    locationsSouthWest = locations[(locations['Long']<Long) & (locations['Lat'] < Lat)] #shops south of warehouse  
    locationsSouthWest = locationsSouthWest[locationsSouthWest['Supermarket'] != 'Warehouse'] # remove the warehouse from the list
    #locationNames = ["North-East", "North-West", "South-East", "South-West"]
    i=0 # counts how many routes
    j = 0
    
    Partitions = [locationsNorthEast,locationsNorthWest, locationsSouthEast, locationsSouthWest]
    for partition in Partitions:
        j+=1
        while (len(partition) != 0): #while ther are still unvisited nodes
            
            currentNode = 'Warehouse' #set node as warehouse, ie beginnning new trip.
            palletsDelivered=0 #set pallets delivered in this trip to 0 (new trip)
            i+=1 # count the routes
            Route = [] # keeps track of the route
            timeLeft = 4*60*60 #The time left in the route for driving only
            RouteTime = 0 # total route time
            Bool = True # whether to continue looping
            Route.append(currentNode) # start the route at the warehouse

            while (palletsDelivered != 12) & (Bool == True): # keep going untill you have 12 pallets or there are no more places to go
                Bool = False
                #find shortest route from current node to next node, which wont go over 12
                minm = 10000 #currentNode is a string of the node name
                to_node = currentNode #set initial/default minimum traveltime value and to-location 
                

                for location in partition['Supermarket']: #go through all locations, location is supermarket name, same format as current node.
                    if ((traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] + 60*5*locations[Day][traveltimes.columns.get_loc(location)-1]) < timeLeft) & (traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] < minm) and (palletsDelivered + locations[Day][traveltimes.columns.get_loc(location)-1] <= 12) and ((currentNode!=location)==True):
                        minm = traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] # set this to min
                        to_node = location # set this node to to_node
                        Bool = True # keep looping



                if Bool == True: # if we found a new node to go to add it to everything
                    palletsDelivered += locations[Day][traveltimes.columns.get_loc(to_node)-1]
                    timeLeft -= minm #Subtracting the driving time
                    timeLeft -= 5*60*locations[Day][traveltimes.columns.get_loc(to_node)-1] #Subtracting the unloading time
                    Route.append(to_node)# append the new node to the route
                    RouteTime += minm # add it to the route time
                    RouteTime += 5*60*locations[Day][traveltimes.columns.get_loc(to_node)-1] # add the unloading time


                currentNode = to_node# change the current node so we can look from there
                partition = partition[partition['Supermarket'] != currentNode] #use conditional indexinf to remove past node from set.

                if (palletsDelivered == 12): # if we have a full truck, stop
                    Bool = False


                if (Bool == False): # if we have finished the route, head back to the warehouse
                    Route.append('Warehouse')
                    RouteTime += traveltimes[to_node][traveltimes.columns.get_loc('Warehouse')-1]
                    totalHours += RouteTime/60/60
                    #append the route to the list of all routes
                    Routes.append(Route)
                    Mins.append(RouteTime/60)
    
    return Routes, Mins

def truck_Rand(Day):
    '''
    This function selects a random node as the starting node from the warehouse and then uses a greedy algorithm to 
    generate feasible routes from then on
    Input: 
        Day: Whether demand should be calculated for Weekdays or Saturdays
    Output: 
        Routes: A list of all the unique feasible routes generated
        Mins: A list of the corresponding route times 
    '''


    cwd = os.getcwd()
    locations = pd.read_csv(cwd + os.sep+ "PythonCode" + os.sep + "ProjectCode" + os.sep + "FoodstuffLocations.csv")
    traveltimes = pd.read_csv(cwd + os.sep+ "PythonCode" + os.sep + "ProjectCode" + os.sep + "FoodstuffTravelTimes.csv")
    Routes = [] # keeps track of all routes
    Mins = [] # keeps trank of all mins
    totalHours = 0
    i=0
    locations = locations[locations['Supermarket'] != 'Warehouse'] # take warehouse out of the nodes

    while (len(locations) != 0): #while ther are still unvisited nodes
        
        currentNode = 'Warehouse' #set node as warehouse, ie beginnning new trip.
        palletsDelivered=0 #set pallets delivered in this trip to 0 (new trip)
        i+=1 # count the routes
        Route = [] # keeps track of the route
        timeLeft = 4*60*60 #The time left in the route for driving only
        RouteTime = 0 # total route time
        Bool = True # whether to continue looping
        Route.append(currentNode) # start the route at the warehouse

        #First run, selecting a random node
        
        AllNames = locations['Supermarket'].to_numpy()        
        currentNode = AllNames[np.random.randint(0,len(AllNames))]

        timeDriving = traveltimes['Warehouse'][traveltimes.columns.get_loc(currentNode)-1]
        Route.append(currentNode)
        timeLeft -= timeDriving
        timeLeft -= 5*60*locations[Day][traveltimes.columns.get_loc(currentNode)-1]
        palletsDelivered += locations[Day][traveltimes.columns.get_loc(currentNode)-1]
        RouteTime += timeDriving # add it to the route time
        RouteTime += 5*60*locations[Day][traveltimes.columns.get_loc(currentNode)-1]
        locations = locations[locations['Supermarket'] != currentNode]
        minm=10000 
        


        while (palletsDelivered != 12) & (Bool == True): # keep going untill you have 12 pallets or there are no more places to go
            Bool = False
            #find shortest route from current node to next node, which wont go over 12
            minm = 10000 #currentNode is a string of the node name
            to_node = currentNode #set initial/default minimum traveltime value and to-location 
            

            for location in locations['Supermarket']: #go through all locations, location is supermarket name, same format as current node.
                if ((traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] < minm) and (palletsDelivered + locations[Day][traveltimes.columns.get_loc(location)-1] <= 12) and ((currentNode!=location)==True)):
                    minm = traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] # set this to min
                    to_node = location # set this node to to_node
                    Bool = True # keep looping



            if Bool == True: # if we found a new node to go to add it to everything
                palletsDelivered += locations[Day][traveltimes.columns.get_loc(to_node)-1]
                timeLeft -= minm #Subtracting the driving time
                timeLeft -= 5*60*locations[Day][traveltimes.columns.get_loc(to_node)-1] #Subtracting the unloading time
                Route.append(to_node)# append the new node to the route
                RouteTime += minm # add it to the route time
                RouteTime += 5*60*locations[Day][traveltimes.columns.get_loc(to_node)-1] # add the unloading time


            currentNode = to_node# change the current node so we can look from there
            locations = locations[locations['Supermarket'] != currentNode] #use conditional indexinf to remove past node from set.
    

            if (palletsDelivered == 12): # if we have a full truck, stop
                Bool = False


            if (Bool == False): # if we have finished the route, head back to the warehouse
                Route.append('Warehouse')
                RouteTime += traveltimes[to_node][traveltimes.columns.get_loc('Warehouse')-1]
                totalHours += RouteTime/60/60
                #append the route to the list of all routes
                Routes.append(Route)
                Mins.append(RouteTime/60)
                
    return Routes, Mins


