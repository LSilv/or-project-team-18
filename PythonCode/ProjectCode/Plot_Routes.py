import numpy as np
import pandas as pd 
import folium
import os
import openrouteservice as ors


def Plot_Routes (routes, Day):

    '''
    This function takes the optimal routes and plots them.
    Inputs:
        routes: a list of all the routes
        Day: whether its is sturday or a weekday
    Outputs:
        None
    '''
    # ORSkey = '5b3ce3597851110001cf6248f9d779f71b6844e590ff51e8035de701'  #Jothams Key
    ORSkey = '5b3ce3597851110001cf6248bf2708af719f464e8247a0131775d50e' #Liams Key
    #ORSkey = '5b3ce3597851110001cf62482a19835bef6d4bbc83f05d940021007f' #Andrews key
    client = ors.Client(key = ORSkey)

    # get the locations
    cwd = os.getcwd()
    locations = pd.read_csv(cwd + os.sep+ "PythonCode" + os.sep + "ProjectCode" + os.sep + "FoodstuffLocations.csv")\


    # get the coordinates for the locations
    coords = locations[['Long', 'Lat']]
    coords = coords.to_numpy().tolist()

    # create the map centred at the warehouse
    m = folium.Map(location = list(reversed(coords[0])),zoom_start=10)


    # in the routes, replace the name with its coordinates
    for i in range(len(routes)):
        for j in range(len(routes[i])):
            x = routes[i][j]
            k = 0
            for location in locations['Supermarket']:
                if x == location:
                    routes[i][j] = []
                    routes[i][j].append(coords[k][0])
                    routes[i][j].append(coords[k][1])
                    break
                k +=1
                    
    

   # loop through the routes and add them to the map
    for i in range(len(routes)):
        for j in range(1,len(routes[i])):

            # create a route
            route1 = client.directions(
                coordinates = [routes[i][j-1], routes[i][j]],
                profile = 'driving-hgv', #Heavy Goods Vehicles routes
                format = 'geojson',
                validate = False

            )
            if i<20:
                #non mainfreight routes
                lineColour = "blue"
                weight = 6
                opacity = 1
            else:
                #mainfreight routes
                lineColour = "red"
                weight = 2
                opacity = 0.9
          
            folium.PolyLine(locations = [list(reversed(coord))
                                    for coord in route1
                                    ['features'][0]['geometry']['coordinates']], color = lineColour, weight = weight, opacity = opacity).add_to(m)
            



    # display the warehouse as black
    folium.Marker(list(reversed(coords[0])), popup = locations.Supermarket[0], icon = folium.Icon(color = 'black')).add_to(m)
    for i in range(1, len(coords)):
        # display new world as red
        if locations.Type[i] == "New World":
            iconCol = "red"
        # paknsave as orange
        elif locations.Type[i] == "Pak 'n Save":
            iconCol = "orange"
            #fresh collective as purple - Now considered a four square
        #elif locations.Type[i] == "Fresh Collective":
         #   iconCol = "purple"
        else:
            # display  four square as green
            iconCol = "green"
        folium.Marker(list(reversed(coords[i])), popup = locations.Supermarket[i], icon = folium.Icon(color = iconCol)).add_to(m)

    # creating the legend with the help of html
    legend_html =   '''
                    <div style="position: fixed; 
                                bottom: 50px; left: 50px; width: 180px; height: 200px; 
                                border:2px solid grey; z-index:9999; font-size:12px;
                                ">&nbsp; Legend: <br>
                                &nbsp; FoodStuff Truck Route   &nbsp; <i class="fa fa-minus fa-1.5x" style="color:blue"></i><br>
                                &nbsp; MainFreight Truck Route &nbsp; <i class="fa fa-minus fa-1.5x" style="color:red"></i><br>
                                &nbsp; FourSquare &nbsp; <i class="fa fa-map-marker fa-2x" style="color:green"></i><br>
                                &nbsp; NewWorld &nbsp; <i class="fa fa-map-marker fa-2x" style="color:red"></i><br>
                                &nbsp; Pak 'n Save &nbsp; <i class="fa fa-map-marker fa-2x" style="color:orange"></i><br>
                                &nbsp; Central Warehouse &nbsp; <i class="fa fa-map-marker fa-2x" style="color:black"></i>
                    </div>
                    ''' 
    # adding legend to the map
    m.get_root().html.add_child(folium.Element(legend_html))
    
    #save the map

    if Day == 'SatDmd':
        m.save('Map_Sat.html')
    else:
        m.save('Map_Week.html')
