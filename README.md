# OR Project Team 18

# Instructions: Run main.py

Number of simulations has been set to 2000 in main.py (line 54). This takes ~1-2 hour to complete. Decrease this value to decrease completion time of code. However, total cost distributions will be less smooth as a result.
