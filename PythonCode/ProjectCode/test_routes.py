
from pulp import*
import pandas as pd
import numpy as np
from IsOnTheRoute import*
import os
from demand_Sim import*
import copy
from RouteJoiner import*


def get_demand_for_store(Store, Day, locations):
    '''
    Helper function to return the demand for a single store
    Inputs:
        Store: which store to get the demand for
        Day: whether it is a weekday or saturday
        locations: a list containing locations and their demands
    Output:
        Demand: the demand for that store on that day
    '''
    storeNames = locations['Supermarket'] #Pandas series of all the supermarket names


    #Searches the storeNames array for the store being considered, and returns its place as an integer index
    storeIndex = storeNames[storeNames == Store].index[0] 
    
    #The indexing is the same in the main array, which contains the demands, so we can use it to access the demand value for the store
    Demand = locations[Day][storeIndex]
    return Demand

def test_routes(routes_original, Day, Mins_original):
    '''
    This function takes the optimal routes generated and tests them against random demands as well 
    as random traffic. This function will slightly alter the routes if the randomly genreated demand 
    makes a route exceed 12 pallets
    Inputs:
        routes_original: the optimal routes generated
        Day: whether it is a weekday or Saturday
        Mins_original: the associated minutes with the optimal routes
    Outputs:
        new_routes: the new routes generated as a result of demand being over 12
        new_Mins: the associated minutes of the new routes
        locations: the locations of stores woth the random demands



    '''
    #To avoid altering the original arrays we take a deep copy
    routes = copy.deepcopy(routes_original)
    Mins = copy.deepcopy(Mins_original)
    cwd = os.getcwd()
    locations = pd.read_csv(cwd + os.sep+ "PythonCode" + os.sep + "ProjectCode" + os.sep + "FoodstuffLocations.csv")
            
   
    for i in range(len(locations.index)):

        brand = locations.at[i,'Type']
        if brand != 'Warehouse':
            #Each store gets a fresh sample from the bootstrapping function
            demands = bootstrap_demands(1) 
            locations.at[i,'SatDmd'] = demands[(demands.Brand == brand ) & (demands.Date == 'Saturday')].Demand
            locations.at[i,'WeekDmd'] = demands[(demands.Brand == brand) & (demands.Date == 'Weekday')].Demand    
  
    
    removed = []
    i = -1
   
          
    for route in routes: 
        visited_stores=[]
        i+=1
        if i == 21:
            print(" ")  
        pallets = 0         #beginning the route, no pallets been delivered

        for store in route[:]:  #It's necessary to iterate over a copy of routes as we remove elements from the list as we iterate

            if store == 'Warehouse':  #If we return to the warehouse partway through the route, we can restock on pallets
                pallets = 0
                visited_stores=[]

            
            #add on each delivery as it is made                
            pallets += get_demand_for_store(store, Day, locations)
            visited_stores.append(store)


            while pallets > 12 :    #if delivery needed is greater than 12, remove the largest demand
                max_dmd = 0
                maxstore = route[0]

                #Finding the store with the maximum demand in the route, to be removed
                for store1 in visited_stores:
                    if get_demand_for_store(store1, Day, locations) > max_dmd:
                        max_dmd = get_demand_for_store(store1, Day, locations)
                        maxstore = store1

                route.remove(maxstore)
                #append the removed node (supermarket) to list so they can be joined in the future
                removed.append(maxstore) 
                pallets -= max_dmd
                #Calculating time for updated route 
                Mins[i] = Get_new_mins(route, locations, Day)

    costs = []

    #If any stores have been removed, new routes are created to connect these nodes
    if (len(removed)!=0):        
        new_routes, new_Mins = route_from_removed(removed, routes, Day, Mins)
        
    else:
        new_routes = routes
        new_Mins = Mins
    
    return new_routes, new_Mins, locations

  


def Get_new_mins(route, DemandsByStore, Day):
    '''
    This function takes a route and returns the time for the route with a random multiplier 
    between 1 and 2 to simulate travel times.
    Inputs:
        route: the route to find the time of
        DemandsByStore: the demand of each store
        Day: whether it is saturday or a wekkday
    Outputs:
        Time: time of the route in mins
    '''
    cwd = os.getcwd()
    #Reading in the names of all the supermarkets
    traveltimes = pd.read_csv(cwd + os.sep+ "PythonCode" + os.sep + "ProjectCode" + os.sep + "FoodstuffTravelTimes.csv")
    i = 0
    currentNode = 'Warehouse'
    time = 0
    
    #Setting traffic multiplier max value
    if Day == 'SatDmd':
        high = 1.5
    else:
        high = 2.0

    for stores in route:
        #The first store is the warehouse, so we skip over it for the first iteration
        if i == 0:
            i +=1

        #Finding time between the current node and the next node
        else:
            toNode = stores
            Traffic = np.random.uniform(low=1.0, high=high)
            time += Traffic * traveltimes[currentNode][traveltimes.columns.get_loc(toNode)-1]            
            currentNode = toNode

    Demand = GetDemand(route, DemandsByStore, Day)
    #Adding loading time
    Loading = Demand*5

    #If the route stops at the warehouse to refill, we also add 5 minutes to load each pallet onto the truck
    if Demand > 12:
        Loading += (Demand-12)*5
    Time = time/60 + Loading
    return Time
    

    


def route_from_removed(removed,routes,Day, mins):
    '''
    This function takes a list of all the nodes which have been removed due to the demand being over
    12 for a route, and generates a new route or routes so they can be visited.
    Inputs:
        removed: a list of all removed nodes
        routes: a list of all routes
        Day: whether it is Saturday or a weekday
        mins: the mins of routes
    Outputs:
        routes: a new list of routes containing the new routes
        mins: a list of mins corresponding to the routes


    '''
    
    cwd = os.getcwd()
    #Reading in the names of all the supermarkets
    locations = pd.read_csv(cwd + os.sep+ "PythonCode" + os.sep + "ProjectCode" + os.sep + "FoodstuffLocations.csv")
    traveltimes = pd.read_csv(cwd + os.sep+ "PythonCode" + os.sep + "ProjectCode" + os.sep + "FoodstuffTravelTimes.csv")
  
    while (len(removed) != 0): #while there are still unvisited nodes
        
        currentNode = 'Warehouse' #set node as warehouse, ie beginnning new trip.
        palletsDelivered=0 #set pallets delivered in this trip to 0 (new trip)
        Route = [] # keeps track of the route
        timeLeft = 4*60*60 #The time left in the route for driving only
        RouteTime = 0 # total route time
        Bool = True # whether to continue looping
        Route.append(currentNode) # start the route at the warehouse

        while (palletsDelivered != 12) & (Bool == True): # keep going untill you have 12 pallets or there are no more places to go
            Bool = False
            #find shortest route from current node to next node, which wont go over 12
            minm = 10000 #currentNode is a string of the node name
            to_node = currentNode #set initial/default minimum traveltime value and to-location 
            

            for location in removed: #go through all locations, location is supermarket name, same format as current node.
                if ((traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] + 60*5*locations[Day][traveltimes.columns.get_loc(location)-1]) < timeLeft) & (traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] < minm) and (palletsDelivered + locations[Day][traveltimes.columns.get_loc(location)-1] <= 12) and ((currentNode!=location)==True):
                    minm = traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] # set this to min
                    to_node = location # set this node to to_node
                    Bool = True # keep looping



            if Bool == True: # if we found a new node to go to add it to everything
                palletsDelivered += locations[Day][traveltimes.columns.get_loc(to_node)-1]
                timeLeft -= minm #Subtracting the driving time
                timeLeft -= 5*60*locations[Day][traveltimes.columns.get_loc(to_node)-1] #Subtracting the unloading time
                Route.append(to_node)# append the new node to the route
                RouteTime += minm # add it to the route time
                RouteTime += 5*60*locations[Day][traveltimes.columns.get_loc(to_node)-1] # add the unloading time


            currentNode = to_node[:] # change the current node so we can look from there
            if currentNode in removed: removed.remove(currentNode) #Remove past node from set.
           

            if (palletsDelivered == 12): # if we have a full truck, stop
                Bool = False


            if (Bool == False): # if we have finished the route, head back to the warehouse
                Route.append('Warehouse')
                RouteTime += traveltimes[to_node][traveltimes.columns.get_loc('Warehouse')-1]
                #append the route to the list of all routes
                routes.append(Route)
                mins.append(RouteTime/60)
    return routes, mins





