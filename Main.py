import sys
import os
import time
import numpy as np
import time
import copy
#Adding our custom functions to the system path
cwd = os.getcwd()
sys.path.append(cwd + os.sep + "PythonCode" + os.sep + "ProjectCode")

from lp_form import*
from GetRoutesAndMins import*
from RouteJoiner import*
from Plot_Routes import*
from demand_Sim import*
from test_routes import*
from conf_int import *



if __name__ == "__main__":
    '''
    This function displays the optimal trucking routes, and the corresponding cost for a given day
    '''
    
    Days = ['WeekDmd', 'SatDmd']
    
    for Day in Days:

        #get routes
        Routes, Mins = GetRoutesAndMins(Day)
    
        #join routes and add to full list of routes
        JoinedRoutes, JoinedMins = JoinRoutes(Routes, Mins,Day)
        Routes.extend(JoinedRoutes)
        Mins.extend(JoinedMins)

    
        # form and solve the liner program
        Optimal_Routes, Optimal_Times, TotalCost = lp_form(Routes, Mins)

        #print out statement to signify end of LP being solved
        if (Day == 'SatDmd'):
            print("\nSuccessfully solved LP routes on Saturdays\n Plotting Routes ...")
        else:
            print("\nSuccessfully solved LP routes on Weekdays\n Plotting Routes ... ")
                
        # plot the optimal routes
        Plot_Routes(copy.deepcopy(Optimal_Routes), Day)
        
        All_Sim_Costs = []


        n = 2000 #Number of iterations to run in the monte carlo simulation
        #Each iteration produces a single simulation
        for i in range(n):

            extra_routes, extra_mins, DemandSim = test_routes(Optimal_Routes, Day, Optimal_Times)
            
            #Updating route times to account for traffic
            for j in range(len(extra_routes)):
                extra_mins[j] = Get_new_mins(extra_routes[j], DemandSim, Day)

            #Resolving LP with new constraints
            Sim_Optimal_Routes, Sim_Optimal_Times, Sim_cost = lp_form(extra_routes, extra_mins)

            All_Sim_Costs.append(Sim_cost)
            print("Iteration number ", i, " out of ", n)
        
            
            
        #Producing distribution of total costs and obtaining confidence and percentile intervals
        conf_int(All_Sim_Costs, Day, n)
        
       

