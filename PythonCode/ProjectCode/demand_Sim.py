import pandas as pd
import os
import numpy as np
import time

def bootstrap_demands(n):
    '''
    This function creates random demand via bootstrapping from the given demand data
    Inputs:
        n : how many samples to generate
    Outputs:
        sampleList: a list of bootstrapped demand for each store type

    '''

    seed = int(np.mod(time.time()*1000, 2**32-1))
    np.random.seed(seed=seed)

    #Reading in our already tidy data from R
    cwd = os.getcwd()
    demands = pd.read_csv(cwd + os.sep + "PythonCode" + os.sep + "ProjectCode" + os.sep + "DemandForSim.csv")

    #Generating a list to store the samples in
    sampleList = pd.DataFrame(columns=['Brand', 'Date', 'Demand'])

    #Looping through each franchise and weekday/saturday
    for brand in demands.Brand.unique():
        for day in demands.Date.unique():

            #Filtering for the selected store/day
            selectedStores = demands[(demands.Brand == brand) & (demands.Date == day)]
            
            #Taking the selected number of samples, with replacement
            samples = selectedStores.sample(n=n, replace = True, random_state = np.random.randint(1, high=9999999))
            sampleList = sampleList.append(samples, ignore_index=True)

    #Importing the csv creates an additional unneeded column
    del sampleList['Unnamed: 0']

    return sampleList

        
        