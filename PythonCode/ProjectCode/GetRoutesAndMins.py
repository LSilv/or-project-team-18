import pandas as pd
import numpy as np
from Truckroutes import*


def GetRoutesAndMins(Day):
    '''
    This function takes the input of the the week day or saturday and returns potential routes generated five different ways,
    by north/south, east/west, no partition, a four way partition, and random routes.
    Input: 
        Day: Whether demand should be calculated for Weekdays or Saturdays
    Output: 
        All_Routes: A list of all the unique feasible routes generated
        All_Mins: A list of the corresponding route times
    '''
    All_Routes = []
    All_Mins = []

    Routes, Mins = truck_EW(Day) # call the method that partitions by east and west
    for route, min in zip(Routes, Mins):
        Bool = True
        for j in range(len(All_Routes)):   # only add the route and time if it is not already there
            if All_Routes[j] == route:
                Bool = False
                break
        if Bool == True:
            All_Routes.append(route)
            All_Mins.append(min)
    Routes, Mins = truck_None(Day) # call the method that has no partition
    for route, min in zip(Routes, Mins):
        Bool = True
        for j in range(len(All_Routes)):   # only add the route and time if it is not already there
            if All_Routes[j] == route:
                Bool = False
                break
        if Bool == True:
            All_Routes.append(route)
            All_Mins.append(min)
    Routes, Mins = truck_NS(Day) # call the method that partitions by north and south 
    for route, min in zip(Routes, Mins):
        Bool = True
        for j in range(len(All_Routes)):   # only add the route and time if it is not already there
            if All_Routes[j] == route:
                Bool = False
                break
        if Bool == True:
            All_Routes.append(route)
            All_Mins.append(min)
    Routes, Mins = truck_4(Day) # call the method that partitions it into 4 parts, North-West, North-East, South-West, South-East
    for route, min in zip(Routes, Mins):
        Bool = True
        for j in range(len(All_Routes)):   # only add the route and time if it is not already there
            if All_Routes[j] == route:
                Bool = False
                break
        if Bool == True:
            All_Routes.append(route)
            All_Mins.append(min)
    #Iniatilizing the random seed so routes are predictably random
    np.random.seed(seed=69)
    # set different iteration number 
    if Day == 'SatDmd':
        iterations = 565
    else:
        iterations = 55
    # create different routes using the random generation
    for i in range (iterations):
        print("\nRandomly Generating Routes #", i+1)
        Routes, Mins = truck_Rand(Day) #
        before = len(All_Routes) # compare how many routes there are before compared to after to see if we are generating new routes
        for route, min in zip(Routes, Mins):
            Bool = True
            for j in range(len(All_Routes)):   # only add the route and time if it is not already there
                if All_Routes[j] == route:
                    Bool = False
                    break
            if Bool == True:
                All_Routes.append(route)
                All_Mins.append(min)

        After = len(All_Routes)
        print("routes added in this iteration =", After-before) # see how mant routes were generated

    print("\nTotal iterations taken =", i+1)
    print("\nTotal Routes = ", len(All_Routes))

    return All_Routes, All_Mins

