import numpy as np
import pandas as pd
import copy
import os

def GetDemand(Route, Loc_Demand, Day):
    '''Takes a Route, from warehouse to warehouse, and finds the demand for pallets along that route
    The Loc_Demand variable, read in from the FoodStuffLocations.csv file, is passed in for memory efficiency (hopefully)
    Inputs: 
        Route: The route being considered, in the format of a list of strings of supermarket names. Inclusive of the warehouse at 
        both ends of the route. 
        Loc_Demand: The imported CSV of all foodstuff locations, with their demand on weekdays and saturdays
        Day: Whether demand should be calculated for Weekdays or Saturdays
    Output: 
        Demand: The number of pallets demanded by stores along the route
    '''
    Demand = 0
    for Store in Route[1:-1]:  #1:-1 because we don't want the warehouses
        
        #Slicing out the column that only contains the names, for ease of indexing later 
        storeNames = Loc_Demand['Supermarket']

        #Searches the storeNames array for the store being considered, and returns its place as an integer index
        storeIndex = storeNames[storeNames == Store].index[0] 

        #The indexing is the same in the main array, which contains the demands, so we can use it to access the demand value for the store
        Demand += Loc_Demand[Day][storeIndex]

    return Demand





def JoinRoutes(Routes, Mins, Day):
    '''
    A function to take two short routes, and join them together, creating a long route that stops at the warehouse halfway through to refill
    E.g if W is the Warehouse and 1,2...etc are supermarkets
    Route 1 is [W,1,2,W], which takes 1 hour
    and Route 2 is [W,3,4,W] takes 1.5 hours
    the routes can be joined to make [W,1,2,W,3,4,W], which takes 3.5 hours (1hr + 1.5hrs + 1hr loading)
    This allows trucks to fulfill multiple routes within a 4 hour shift
    Returns only the joined routes (And their respective times) - these should be appended to the main list of routes
    Inputs: 
        Routes: A list of all feasible routes that have been generated
        Mins: A list of the time taken in minutes to complete each route, inclusive of loading times. This is indexed to match the Routes list (above)
            so Mins[x] is the time taken to complete Routes[x]
        Day: Whether we are calculating routes for Saturday ("SatDmd") or Weekdays ("WeekDmd"). Used to calculate the demand, and hence time taken
    Outputs: 
        JoinedRoutes: A list of the newly generated joined routes. Does not contain the original routes array, 
            so should be added to this outside the function
        JoinedMins: A list of the time taken in minutes to complete each route in the above array, indexed such that JoinedMins[x] is 
            the time taken to complete JoinedRoutes[x]

    '''

   

    #Python assignment requires that we use the copy operator to avoid changing the original list
    #We convert from lists to numpy arrays so we can use logical indexing
    Mins_NP = copy.deepcopy(np.asarray(Mins)) 
    Routes_NP = copy.deepcopy(np.asarray(Routes))

   
    #First we find all routes under 1.5 hours
    #this ensures that even with the worst case scenario 
    #(2x 1.5 hour routes, 12 pallets being loaded at warehouse), the total
    #Route time is under 4 hours
    #This restriction could be relaxed for a greater variety of routes, and lengthy routes would 
    #simply be penalised by the overtime cost in the LP. However, that would generate a huge 
    #amount of routes, most of which would never be selected (as they are penalised for being long)
    ShortRoutes = Routes_NP[Mins_NP<=1.5*60]
    ShortMins = Mins_NP[Mins_NP<=1.5*60]
    
    #Locations with demands - used to calculate the loading time at the warehouse 
    cwd = os.getcwd()
    loc_Demand = pd.read_csv(cwd + os.sep+ "PythonCode" + os.sep + "ProjectCode" + os.sep + "FoodstuffLocations.csv") 

    #Creating empty arrays to store the newly created routes, and the time taken to complete them
    #Times are inclusive of loading and unloading time
    JoinedRoutes=[]
    JoinedMins=[]

    #This outer loop runs through all the Routes that take less than 1.5 hours, except the last route
    #As the last route will already by joined to all the prior routes (assuming symmetrical travel times)
    for i in range(len(ShortRoutes)):

        FirstRoute = ShortRoutes[i]

        #We remove the warehouse from the end of the list - we don't need to go there twice
        del FirstRoute[-1]   

        #This loop runs through all the routes that come AFTER the first route
        #We assume route timings are symmetrical, so routes aren't joined to the routes prior in the list
        #As they have already been joined when the prior routes were selected by the outer loop
        for j in range(i+1,len(ShortRoutes)):
            SecondRoute = ShortRoutes[j]            
            Dem = GetDemand(SecondRoute, loc_Demand, Day)
            LoadTime = 5*Dem #Each pallet takes 5 minutes to load at the warehouse
            TotalTime = ShortMins[i] + ShortMins[j] + LoadTime
            JoinedRoutes.append(FirstRoute+SecondRoute)
            JoinedMins.append(TotalTime)




    return JoinedRoutes, JoinedMins 
