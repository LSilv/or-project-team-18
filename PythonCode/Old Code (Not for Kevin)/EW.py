
import pandas as pd

locations = pd.read_csv("Data/FoodstuffLocations.csv")
traveltimes = pd.read_csv("Data/FoodstuffTravelTimes.csv")
Routes = [] # keeps track of all routes
Mins = [] # keeps trank of all mins
totalHours = 0 # toatal hours

locationsEast = locations[(locations['Long']>174.7281)] #Shops north of warehouse
locationsWest = locations[(locations['Long']<174.7281)] #shops south of warehouse 
locationsEast = locationsEast[locationsEast['Supermarket'] != 'Warehouse'] # remove the warehouse from the list
i=0 # counts how many routes
Day = 'WeekDmd'  #Whether to calculate routes for the Weekdays ('WeekDmd') or on Saturdays ('SatDmd')

print("\nCalculating routes East of the warehouse \n")
while (len(locationsEast) != 0): #while ther are still unvisited nodes
    
    currentNode = 'Warehouse' #set node as warehouse, ie beginnning new trip.
    palletsDelivered=0 #set pallets delivered in this trip to 0 (new trip)
    i+=1 # count the routes
    Route = [] # keeps track of the route
    timeLeft = 4*60*60 #The time left in the route for driving only
    RouteTime = 0 # total route time
    Bool = True # whether to continue looping
    Route.append(currentNode) # start the route at the warehouse

    while (palletsDelivered != 12) & (Bool == True): # keep going untill you have 12 pallets or there are no more places to go
        Bool = False
        #find shortest route from current node to next node, which wont go over 12
        minm = 10000 #currentNode is a string of the node name
        to_node = currentNode #set initial/default minimum traveltime value and to-location 
        

        for location in locationsEast['Supermarket']: #go through all locations, location is supermarket name, same format as current node.
            #    print(location)
            if ((traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] + 60*5*locations[Day][traveltimes.columns.get_loc(location)-1]) < timeLeft) & (traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] < minm) and (palletsDelivered + locations[Day][traveltimes.columns.get_loc(location)-1] <= 12) and ((currentNode!=location)==True):
                minm = traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] # set this to min
                to_node = location # set this node to to_node
                Bool = True # keep looping



        if Bool == True: # if we found a new node to go to add it to everything
            palletsDelivered += locations[Day][traveltimes.columns.get_loc(to_node)-1]
            timeLeft -= minm #Subtracting the driving time
            timeLeft -= 5*60*locations[Day][traveltimes.columns.get_loc(to_node)-1] #Subtracting the unloading time
            Route.append(to_node)# append the new node to the route
            RouteTime += minm # add it to the route time
            RouteTime += 5*60*locations[Day][traveltimes.columns.get_loc(to_node)-1] # add the unloading time


        currentNode = to_node# change the current node so we can look from there
        locationsEast = locationsEast[locationsEast['Supermarket'] != currentNode] #use conditional indexinf to remove past node from set.

        if (palletsDelivered == 12): # if we have a full truck, stop
            Bool = False


        if (Bool == False): # if we have finished the route, head back to the warehouse
            Route.append('Warehouse')
            RouteTime += traveltimes[to_node][traveltimes.columns.get_loc('Warehouse')-1]
            #print the route
            print("Route", i, "is", Route, "and takes", round(RouteTime/60,2), "Minutes and delivers", palletsDelivered, "pallets")
            totalHours += RouteTime/60/60
            #append the route to the list of all routes
            Routes.append(Route)
            Mins.append(RouteTime/60)



## south
print("\nCalculating routes West of the warehouse \n")

while (len(locationsWest) != 0): #while ther are still unvisited nodes
    
    currentNode = 'Warehouse' #set node as warehouse, ie beginnning new trip.
    palletsDelivered=0 #set pallets delivered in this trip to 0 (new trip)
    i+=1 # count the routes
    Route = [] # keeps track of the route
    timeLeft = 4*60*60 #The time left in the route for driving only
    RouteTime = 0 # total route time
    Bool = True # whether to continue looping
    Route.append(currentNode) # start the route at the warehouse

    while (palletsDelivered != 12) & (Bool == True):
        Bool = False
        #find shortest route from current node to next node, which wont go over 12
        minm = 10000 #currentNode is a string of the node name
        to_node = currentNode #set initial/default minimum traveltime value and to-location 
        

        for location in locationsWest['Supermarket']: #go through all locations, location is supermarket name, same format as current node.
            #    print(location)
            if ((traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] + 60*5*locations[Day][traveltimes.columns.get_loc(location)-1]) < timeLeft) & (traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] < minm) and (palletsDelivered + locations[Day][traveltimes.columns.get_loc(location)-1] <= 12) and ((currentNode!=location)==True):
                minm = traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] #set this value to the min
                to_node = location # set this node to the node
                Bool = True # continue looping



        if Bool == True:  # if we found a new node to go to add it to everything
            palletsDelivered += locations[Day][traveltimes.columns.get_loc(to_node)-1] # add the pallets
            timeLeft -= minm #Subtracting the driving time
            timeLeft -= 5*60*locations[Day][traveltimes.columns.get_loc(to_node)-1] #Subtracting the unloading time
            Route.append(to_node) # add the node to the route
            RouteTime += minm # add to the route time
            RouteTime += 5*60*locations[Day][traveltimes.columns.get_loc(to_node)-1] # add the unloading time


        currentNode = to_node # set this node to the current node
        locationsWest = locationsWest[locationsWest['Supermarket'] != currentNode] #use conditional indexinf to remove past node from set.

        if (palletsDelivered == 12): # if the truck is full stop looping
            Bool = False 

        if (Bool == False): # if the route is finished head back to the warehouse
            Route.append('Warehouse')
            RouteTime += traveltimes[to_node][traveltimes.columns.get_loc('Warehouse')-1]
            print("Route", i, "is", Route, "and takes", round(RouteTime/60,2), "Minutes and delivers", palletsDelivered, "pallets")
            totalHours += RouteTime/60/60
            Routes.append(Route) # append this route to the list of all routes
            Mins.append(RouteTime/60)

print("The total time taken in hours is", round(totalHours,2))
print("In minutes is", round(totalHours*60,2))
print("In seconds is", round(totalHours*60*60,2))
