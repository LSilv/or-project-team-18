def IsOnTheRoute(Route, SuperMarket):
    '''
    This function checks whether a particular supermarket is on a particular route
    Input: 
        Route: a list of strings which contains the route to look through
        SuperMarket: a string containing the supermarket to look for
    Output: 
        Boolean: True if it is on the route, false if not
    '''

    # finds if a supermarket is on a specific route
    for places in Route:
        # if it is on the route return true
        if places == SuperMarket:
            return True
    #otherwise return false
    return False

