from scipy.stats import norm 
import matplotlib.pyplot as plt
import numpy as np
import statsmodels.stats.weightstats as sms
import datetime

def conf_int(cost, Day, n):
    '''
    This function takes all the costs after multiple simulations are done and produces a graph showing a 
    histogram of results and displaying a 95% confidence interval and percentile interval on the graph. Either saves
    or displays to screens
    Inputs:
        cost: a list of all the costs from the simulations
        Day: whether it is saturday or a weekday
    Outputs:
        None

    '''
    num_bins = 50
    # create figure and axes
    fig, ax = plt.subplots(figsize=(12,6))
    # probability distribution graph
    ax.hist(cost, num_bins, density=True, histtype='stepfilled', alpha=0.75, color='steelblue')

    # calculate mean and standard deviation of distribution of gamma
    cost_mean = np.mean(cost)
    cost_sd = np.std(cost)

    # calculating 95% confidence and percentile interval   
    cost.sort()
    start = cost[int(len(cost)*0.025)]
    end = cost[int(len(cost)*0.975)]
    print('Percentile Interval for Total Cost:',[start, end])
    weekLower, weekUpper = sms.DescrStatsW(cost).tconfint_mean(alpha=0.05)
    print('Confidence Interval for Total Cost:',[weekLower, weekUpper])
    

    # plotting of confidence/Percentile interval lines
    ax.axvline(start, color='r', linestyle=':', label='95% Percentile Interval')
    ax.axvline(end, color='r', linestyle=':')
    ax.axvline(weekLower, color='g', linestyle=':', label='95% Confidence Interval')
    ax.axvline(weekUpper, color='g', linestyle=':')
    # Labelling and legend
    ax.set_xlabel('Total Cost', fontsize = '12')
    ax.set_ylabel('Probability density', fontsize = '12')

    # Create empty plot with blank marker containing the extra labels
    plt.plot([], [], ' ', label="Mean: " + format(cost_mean, '.2f'))    
    plt.plot([], [], ' ', label="St. Dev:" + format(cost_sd, '.2f'))
    plt.plot([], [], ' ', label="Lower bound of PI: " + format(start, '.2f'))
    plt.plot([], [], ' ', label="Upper bound of PI: " + format(end, '.2f'))
    plt.plot([], [], ' ', label="Lower bound of CI: " + format(weekLower, '.2f'))
    plt.plot([], [], ' ', label="Upper bound of CI: " + format(weekUpper, '.2f'))
    ax.legend(loc='upper right')
    
    # save and show
    plt.title("Distribution of costs for " + Day + ", " + str(n) + " iterations")
    save_figure = True
    if not save_figure:
        plt.show()
    else:
        timeDT = datetime.datetime.now()
        plt.savefig('Distributions/' + str(n)+"_Iterations"+ Day + (timeDT.strftime("%m%d_%H%M"))+'Cost_Dist.png',dpi=300)



