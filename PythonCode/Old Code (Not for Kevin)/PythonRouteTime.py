# -*- coding: utf-8 -*-
"""
Created on Thu Oct  3 14:49:23 2019

@author: harris


"""

#Comment/Uncomment the keys if we run out of API requests
#ORSkey = '5b3ce3597851110001cf6248f9d779f71b6844e590ff51e8035de701'  #Jothams Key
ORSkey = '5b3ce3597851110001cf6248bf2708af719f464e8247a0131775d50e' #Liams Key
#ORSkey = '5b3ce3597851110001cf62482a19835bef6d4bbc83f05d940021007f' #Andrews key


import numpy as np
import pandas as pd 
import folium
import openrouteservice as ors
from time import sleep
locations = pd.read_csv("Data/FoodstuffLocations.csv") #read in the data

#lat of warehouse is -36.90790

#split into north of warehouse and south of warehouse

locationsNorth = locations[locations['Lat']>=-36.907904]
locationsSouth = locations[locations['Lat']<=-36.907904]
#print(locationsNorth)

    



supieNamesNorth = locationsNorth['Supermarket'] #extract just supermarket names, for later use

#Extracting a list of supermarket names, not including pak 'n save mt albert
locations_PS = locations[locations['Supermarket']!="Pak 'n Save Mt Albert"]
supieNames = locations_PS['Supermarket']

PNS = locations[locations['Supermarket']=="Pak 'n Save Mt Albert"][['Long','Lat']]
PNS = PNS.to_numpy().tolist()
print(PNS[0])
coords = locations_PS[['Long','Lat']] #extract just location info 
coords = coords.to_numpy().tolist() #co-ords turn into a numpy list of lat&long
print(coords[2])


for i in range(len(supieNames)):
    client = ors.Client(key=ORSkey) #key for accessing openrouteservice   
    if (i==39):
        sleep(60) #We're limited to 40 requests per minute     
    route = client.directions(
            [coords[i], PNS[0]], #specify  end supermarket node
                
            profile = 'driving-hgv', #get only driving routes
            format='geojson',
            validate = False
            )  
    #print("distance between ",supieNames.iloc[i]," and Pak 'n Save Mt Albert is ",route['features'][0]['properties']['summary']['distance'],"m")
    print("time to travel from Pak 'n Save Mt Albert to",supieNames.iloc[i], "is ",route['features'][0]['properties']['summary']['duration'],"seconds")



# for i in range(len(supieNamesNorth)): #compare one supermarket...
#     for j in range(i+1,len(supieNamesNorth)): #...to another supermarket, with no overlap
#         client = ors.Client(key=ORSkey) #key for accessing openrouteservice
        
#         route = client.directions(
#                 [coords[i], coords[j]], #specify beginning and end supermarket nodes
                
#                 profile = 'driving-hgv', #get only driving routes
#                 format='geojson',
#                 validate = False
#                 )
       

#         print("distance between ",supieNamesNorth.iloc[i]," and ",supieNamesNorth.iloc[j], " is ",route['features'][0]['properties']['summary']['distance'],"m")
#         print("time to travel between ",supieNamesNorth.iloc[i], " and ",supieNamesNorth.iloc[j]," is ",route['features'][0]['properties']['summary']['duration'],"seconds")

# supieNamesSouth =  locationsSouth['Supermarket']
# for i in range(len(supieNamesSouth)): #compare one supermarket...
#     for j in range(i+1,len(supieNamesSouth)): #...to another supermarket, with no overlap
#         client = ors.Client(key=ORSkey) #key for accessing openrouteservice
        
#         route = client.directions(
#                 [coords[i], coords[j]], #specify beginning and end supermarket nodes
                
#                 profile = 'driving-hgv', #get only driving routes
#                 format='geojson',
#                 validate = False
#                 )
        
        #print("distance between ",supieNames[i]," and ",supieNames[j], " is ",route['features'][0]['properties']['summary']['distance'],"m")
        #print("time to travel between ",supieNames[i], " and ",supieNames[j]," is ",route['features'][0]['properties']['summary']['duration'],"seconds")
