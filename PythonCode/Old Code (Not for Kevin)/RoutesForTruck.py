# -*- coding: utf-8 -*-
"""
Created on Tue Oct  8 15:36:26 2019

@author: harri
"""

#from pulp import*
import pandas as pd

#HAVE ADDED 2 more colunmns to Foodstufflocations (the excel file). These are WkDmd (week demand) and SatDmd (demand on saturday)
#for whatever model we end up with it will run 4 times

#Northlocations on a weekday
#Northlocations on a saturday

#Southlocations weekday
#Southlocations Saturday

locations = pd.read_csv("Data/FoodstuffLocations.csv")
traveltimes = pd.read_csv("Data/FoodstuffTravelTimes.csv")

Route = []

locationsNorth = locations[(locations['Lat']>=-36.907904)] #Shops north of warehouse, including warehouse
locationsSouth = locations[(locations['Lat']<=-36.907904)] #shops south of warehouse incl warehouse
i=0

while len(locationsNorth) != 0: #while ther are still unvisited nodes
    
    currentNode = 'Warehouse' #set node as warehouse, ie beginnning new trip.
    palletsDelivered=0 #set pallets delivered in this trip to 0 (new trip)
    timeLeft = 4*60*60 #The time left in the route for driving only
    i+=1
    print(Route)
    Route = []
    RouteTime = 0
    Bool = True


    while (palletsDelivered != 12) & (Bool == True):
        Bool = False
        #find shortest route from current node to next node, which wont go over 12
        minm = 10000 #currentNode is a string of the node name
        to_node = currentNode #set initial/default minimum traveltime value and to-location 
        
        for location in locationsNorth['Supermarket']: #go through all locations, location is supermarket name, same format as current node.
            print(location)
            Demand = locationsNorth['WeekDmd'][traveltimes.columns.get_loc(location)-1]
            if ((traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] + 60*5*Demand) < timeLeft) and (palletsDelivered + locationsNorth['WeekDmd'][traveltimes.columns.get_loc(location)-1] <= 12) and ((currentNode!=location)==True):
                minm = traveltimes[currentNode][traveltimes.columns.get_loc(location)-1] #
                timeLeft -= minm #Subtracting the driving time
                timeLeft -= 5*60*Demand #Subtracting the unloading time
                to_node = location
                Bool = True
                
        palletsDelivered += locationsNorth['WeekDmd'][traveltimes.columns.get_loc(location)-1]
     #   print(locationsNorth['WeekDmd'][traveltimes.columns.get_loc(location)-1])
        Route.append(currentNode)
        RouteTime += minm

        currentNode = to_node
        try:
            locationsNorth = locationsNorth[locationsNorth['Supermarket'] != currentNode] #use conditional indexinf to remove past node from set.
            
        except:
            locationsNorth = locationsNorth
            #catches error if we try to delete warehouse which isnt in list   
             
        print("calculation made for route delivery number ",i)
        #get index (number) from Supermarket name 
